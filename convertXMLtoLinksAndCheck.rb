require 'rubygems'
require 'xmlsimple'
require 'net/https' #use https or http depending on use- also see code below

checkedLinks = 0
brokenLinks = 0
program = 'dc_cc2' #It's easiest to name the xml file this and then let the variable take care of everything
fileoutName = ".//PicLinks\\AllAPT#{program}PicsSol5.html" #folder 'PicLinks' should exist in the directory the script is run
fileout = File.open(fileoutName, "w")
fileout.puts "<!DOCTYPE html><html><head><title>APT - All Image Links</title></head><body>"

fileoutNameNotFound = ".//PicLinks\\AllAPT#{program}PicsNotFoundSol5.html"
fileoutNotFound = File.open(fileoutNameNotFound, "w")
fileoutNotFound.puts "<!DOCTYPE html><html><head><title>APT- All Image Links Not Found</title></head><body>"

isMiddleSchool = true
p program;
if(isMiddleSchool) #Middle school has the images in a data folder
baseUrl = "https://static.bigideasmath.com/protected/content/#{program}/apt/data/images/"
else
baseUrl = "https://static.bigideasmath.com/protected/content/#{program}/apt/images/"
end
#specify where the xml is, again it is best to name the file the same as the program, which is what is in the url
Dir.glob("C:/Users/jklins/Documents/sandbox/AnswerPresentationTool/final/xml/#{program}.xml") do |item|
  lastErrorItem = ''
  lastErrorChapAndSection = ''
  fileout.puts "<h2>#{item}</h2>"
  myXml = XmlSimple.xml_in(item)

  myXml["grade"].each do |gr|
	gr["chapter"].each do |chap|
		chap["section"].each do |sec|
			for i in 1..sec["num_ex"].to_i
				checkedLinks += 1
				if (isMiddleSchool)
					if (chap["name"] == "Additional Topics")
						chapName = 'at'
						secName = sec['name'][0,2].to_i.to_s.rjust(2, '0') #dealing with things like '1 - On Your Own'
					else 
						chapName = chap["name"].to_s.rjust(2, '0')
						secName = sec["prefix"][4,2]
					end
					case gr["name"]
						when "Course 2 Accelerated"
							grade = 'g13'
						when "Red Accelerated"
							grade = 'g13'
						when "Advanced 1"
							grade = 'g11'
						when "Advanced 2"
							grade = 'g12'
						else
							grade = sec["prefix"][0,2]
					end
					secondUrl = "#{grade}/#{chapName}/#{secName}/"
					baseAddress = baseUrl + secondUrl
					imgName = "s_#{sec["prefix"]}#{i}.gif" #if checking solutions add 's_' to imgName
				else	
					splitter = sec["prefix"].split("_")
					secondUrl = "#{splitter[-4]}/#{splitter[-2]}/#{splitter[-1]}/"
					baseAddress = baseUrl + secondUrl
					paddedNum = "%03d" % i
					imgName = "s_#{sec["prefix"]}_#{paddedNum}.png" #solutions start with 's_' answers start with 'a_'
				end
				imgUrl = "#{baseAddress}#{imgName}"
				fileout.puts "#{chap["name"]}.#{sec["name"]} ex #{i} | <a href =#{imgUrl}>#{imgUrl}</a><br/>"
					
					
				###
				uri = URI(imgUrl)
				req = Net::HTTP::Get.new(uri.path)
				res = Net::HTTP.start(
					uri.host, uri.port, 
					:use_ssl => uri.scheme == 'https', 
					:verify_mode => OpenSSL::SSL::VERIFY_NONE) do |https|
					https.request(req)
				end
				###Use the commented out stuff if not checking https links - also change Net/Https at the top to Net/Http
				#url = URI.parse(imgUrl)
				#req = Net::HTTP.new(url.host, url.port)
				#res = req.request_head(url.path)
				if res.code != '200'
					if lastErrorItem != item
						fileoutNotFound.puts "<h2>#{item}</h2>"
						lastErrorItem = item
						p item 
					end
					if lastErrorChapAndSection != "#{chap["name"]}.#{sec["name"]}"
						fileoutNotFound.puts "<h2>#{chap["name"]}.#{sec["name"]}</h2>"
						lastErrorChapAndSection = "#{chap["name"]}.#{sec["name"]}"
					end
					p "exercise missing (#{res.code}): #{chap["name"]}.#{sec["name"]} ex: #{i} | #{imgUrl}"
					brokenLinks += 1
					fileoutNotFound.puts "exercise missing (#{res.code}): #{chap["name"]}.#{sec["name"]} ex: #{i} | <a href =#{imgUrl}>#{imgUrl}</a><br/>"
				end
			end 
		end
	end
  end
end
 fileout.puts "<h2>Total Checked Links: #{checkedLinks}</h2>"
 fileout.puts "<h2>Total Broken Links: #{brokenLinks}</h2>"
 fileout.puts "See <a href = 'AllCalcChatPicsNotFound.html'>AllCalcChatPicsNotFound.html</a> For Specific Info Related to Links without 200 code returns"
 fileout.puts "</body></html>"
 p "Total Checked Links: #{checkedLinks}"
 p "Total Broken Links: #{brokenLinks}"
 fileout.close
 fileoutNotFound.puts "<h2 style='color:blue'>Total Checked Links: #{checkedLinks}</h2>"
 fileoutNotFound.puts "<h2 style='color:red'>Total Broken Links: #{brokenLinks}</h2>"
 fileoutNotFound.puts "</body></html>"
 fileoutNotFound.close