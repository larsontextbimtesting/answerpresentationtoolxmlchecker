It will help to read the comments in the code.
Ruby needs installed on the machine running it.

This takes xml that is copied from the source of the answer presentation tool html files, converts them to links and verifies they are valid
-There is some issue where this does not work well with California (this is a result of not having a logged-in cookie - all links checked by using the html file, logging in and then using a browser extension)
It is designed to be looking for an xml file in the xml directory that has the short name of the program that contains it.
eg. dc_cc2.xml would be used for the xml that is in common core middle school.
The program then outputs 2 html files in the PicLinks folder. One that has all the links, and one that has links that failed in the automated checking so that they may be viewed manually.

I accidentally committed a ton of these html files upon initializing the repository. Some are incomplete as the commit was done as I was running the tests. Sorry bout the clutter in the PicLinks folder.